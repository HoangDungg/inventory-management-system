using HD.LegalIMS.EntityFramework.Repositories;
using HD.LegalIMS.Application.Interfaces.EntityFramework;
using Microsoft.Extensions.DependencyInjection;

namespace HD.LegalIMS.EntityFramework
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEntityFrameworkGenericRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(ICommandRepository<>), typeof(CommandRepository<>));
            services.AddScoped(typeof(IQueryRepository<>), typeof(QueryRepository<>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}