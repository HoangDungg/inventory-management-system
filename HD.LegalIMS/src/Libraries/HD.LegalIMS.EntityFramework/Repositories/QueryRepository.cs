﻿using HD.LegalIMS.Application.Interfaces.EntityFramework;
using HD.LegalIMS.Services.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HD.LegalIMS.EntityFramework.Repositories
{
    public class QueryRepository<TEntity> : IQueryRepository<TEntity> where TEntity : class
    {
        protected readonly SourceContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public QueryRepository(SourceContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        public IList<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> includeEntites = null, bool disableChangeTracker = true)
        {
            return InitQuery(filter, includeEntites, disableChangeTracker).ToList();
        }

        public async Task<IList<TEntity>> GetAllAsync(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> includeEntities = null, bool disableChangeTracker = true)
        {
            return await InitQuery(filter, includeEntities, disableChangeTracker).ToListAsync();
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> includeEntities = null, bool disableChangeTracker = true)
        {
            if (filter == null)
            {
                throw new ArgumentNullException(nameof(filter));
            }
            return InitQuery(filter, includeEntities, disableChangeTracker);
        }

        public IQueryable<TEntity> InitQuery(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> includeEntities = null, bool disableChangeTracker = true)
        {
            IQueryable<TEntity> query = _dbSet.AsQueryable();
            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeEntities != null)
            {
                query = query.Include(includeEntities);
            }

            if (disableChangeTracker)
            {
                query = query.AsNoTracking();
            }
            return query;
        }
    }
}