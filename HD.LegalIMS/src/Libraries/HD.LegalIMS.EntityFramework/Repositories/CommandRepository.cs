﻿using HD.LegalIMS.Application.Interfaces.EntityFramework;
using HD.LegalIMS.Services.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;

namespace HD.LegalIMS.EntityFramework.Repositories
{
    public class CommandRepository<TEntity> : ICommandRepository<TEntity> where TEntity : class
    {
        protected readonly SourceContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public CommandRepository(SourceContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        public void Add(params TEntity[] entities)
        {
            PerformDbOperation(_dbSet.Add, entities);
        }

        public void Delete(params TEntity[] entities)
        {
            PerformDbOperation(_dbSet.Remove, entities);
        }

        public void Update(params TEntity[] entities)
        {
            PerformDbOperation(_dbSet.Update, entities);
        }

        private void PerformDbOperation(Func<TEntity, EntityEntry<TEntity>> dbOperation, params TEntity[] entities)
        {
            if (dbOperation == null)
            {
                throw new ArgumentNullException(nameof(dbOperation));
            }

            if (entities == null || entities.Any(x => x == null))
            {
                throw new ArgumentException(nameof(entities));
            }

            foreach (TEntity entity in entities)
            {
                dbOperation(entity);
            }
        }
    }
}