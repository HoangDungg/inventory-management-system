﻿using HD.LegalIMS.Application.Interfaces.EntityFramework;
using HD.LegalIMS.Services.Infrastructure.Data;
using System.Threading;
using System.Threading.Tasks;

namespace HD.LegalIMS.EntityFramework.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SourceContext _dbContext;

        public UnitOfWork(SourceContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}