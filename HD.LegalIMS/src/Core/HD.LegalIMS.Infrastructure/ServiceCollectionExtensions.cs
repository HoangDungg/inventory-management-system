using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace HD.LegalIMS.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCoreDependencies(this IServiceCollection services, Assembly[] assemblies)
        {
            services.AddMediatR(assemblies);
            return services;
        }
    }
}