using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using HD.LegalIMS.API.Extensions;
using HD.LegalIMS.Application;
using HD.LegalIMS.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HD.LegalIMS.API.Middleware
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            object exceptionHandler = httpContext.RequestServices.GetExceptionHandler(exception);

            ProblemDetails problemDetails = null;

            if (exceptionHandler != null)
            {
                System.Reflection.MethodInfo method = exceptionHandler.GetType().GetMethod(nameof(IExceptionHandler<Exception, ProblemDetails>.Handle));
                if (method != null)
                {
                    problemDetails = (ProblemDetails)method.Invoke(exceptionHandler, new object[] { exception });
                }
            }

            if (problemDetails == null)
            {
                problemDetails = new ProblemDetails {
                    Status = (int)HttpStatusCode.InternalServerError,
                    Title = Constants.RequestHandling.Messages.UnhandledExceptionTitle,
                    Detail = exception.ToString()
                };
            }

            problemDetails.Instance = httpContext.Request.Path;
            httpContext.Response.ContentType = "application/json";

            httpContext.Response.StatusCode = problemDetails.Status.GetValueOrDefault((int)HttpStatusCode.BadRequest);
            await httpContext.Response.WriteAsync(JsonSerializer.Serialize<object>(problemDetails));
        }
    }
}