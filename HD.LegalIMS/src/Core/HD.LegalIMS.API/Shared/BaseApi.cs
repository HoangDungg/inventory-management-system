﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using HD.LegalIMS.Core.Shared;
using HD.LegalIMS.Core;

namespace HD.LegalIMS.API.Shared
{
    [ApiController]
    public class BaseApi : ControllerBase
    {
        //protected readonly ICommonComponents _commonComponents;
        protected readonly IMediator _mediator;

        protected BaseApi(IMediator mediator)
        {
            _mediator = mediator;
        }

        protected Response<T> Success<T>(string message = Constants.RequestHandling.Messages.Success, HttpStatusCode statusCode = HttpStatusCode.OK, T data = default)
        {
            return new Response<T>(message, statusCode, data: data);
        }

        protected Response Success(string message = Constants.RequestHandling.Messages.Success, HttpStatusCode statusCode = HttpStatusCode.OK)
        {
            return new Response(message, statusCode);
        }
    }
}