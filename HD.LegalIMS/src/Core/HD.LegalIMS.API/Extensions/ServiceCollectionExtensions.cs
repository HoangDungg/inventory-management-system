using System;
using System.Text.Json;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace HD.LegalIMS.API.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IMvcBuilder AddWebAPICore(this IServiceCollection services)
        {
            IMvcBuilder mvcBuilder = services.AddRouting(x => x.LowercaseUrls = true)
                .AddControllers()
                .AddJsonOptions(x =>
                {
                    x.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                });

            services.AddResponseCaching()
                .AddHttpContextAccessor();
            return mvcBuilder;
        }

        public static IServiceCollection AddApiVersioningCore(this IServiceCollection services)
        {
            services.AddApiVersioning(opts =>
            {
                opts.DefaultApiVersion = new ApiVersion(1, 0);
                opts.AssumeDefaultVersionWhenUnspecified = true;
            });
            return services;
        }

        public static IServiceCollection AddCookie(this IServiceCollection services)
        {
            services.ConfigureApplicationCookie(o =>
            {
                o.ExpireTimeSpan = TimeSpan.FromDays(5);
                o.SlidingExpiration = true;
            });

            services.Configure<DataProtectionTokenProviderOptions>(o =>
            {
                o.TokenLifespan = TimeSpan.FromHours(3);
            });
            return services;
        }
    }
}