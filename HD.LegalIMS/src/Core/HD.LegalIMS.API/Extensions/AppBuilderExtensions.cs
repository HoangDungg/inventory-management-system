using System;
using HD.LegalIMS.Application;
using Microsoft.AspNetCore.Mvc;

namespace HD.LegalIMS.API.Extensions
{
    public static class AppBuilderExtensions
    {
        public static object GetExceptionHandler(this IServiceProvider serviceProvider, Exception exception)
        {
            Type concreteType = typeof(IExceptionHandler<,>).MakeGenericType(exception.GetType(), typeof(ProblemDetails));

            object exceptionHandler = serviceProvider.GetService(concreteType);

            if (exceptionHandler == null)
            {
                exceptionHandler = serviceProvider.GetService(typeof(IExceptionHandler<Exception, ProblemDetails>));
            }
            return exceptionHandler;
        }
    }
}