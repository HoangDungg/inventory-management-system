using FluentValidation;
using FluentValidation.Results;
using HD.LegalIMS.Core;

namespace HD.LegalIMS.Application.RequestValidator
{
    public abstract class BaseRequestValidator<TRequest> : AbstractValidator<TRequest>
    {
        protected override bool PreValidate(ValidationContext<TRequest> context, ValidationResult result)
        {
            bool isContinueTheValidation = true;

            if (context.InstanceToValidate == null)
            {
                result.Errors.Add(new ValidationFailure(string.Empty, Constants.RequestHandling.Messages.InvalidRequest));
                isContinueTheValidation = false;
            }
            return isContinueTheValidation;
        }
    }
}