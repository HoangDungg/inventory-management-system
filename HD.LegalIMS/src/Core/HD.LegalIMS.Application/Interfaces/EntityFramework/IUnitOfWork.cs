using System.Threading;
using System.Threading.Tasks;

namespace HD.LegalIMS.Application.Interfaces.EntityFramework
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}