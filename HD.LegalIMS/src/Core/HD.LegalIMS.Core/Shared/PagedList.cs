﻿using System.Collections.Generic;
using System.Linq;

namespace HD.LegalIMS.Core.Shared
{
    public class PagedList<T>
    {
        public PagedList()
        {
        }

        public PagedList(IQueryable<T> query, int? pageIndex = null, int? pageSize = null)
        {
            PageIndex = pageIndex ?? 1;
            PageSize = !pageSize.HasValue || pageSize == 0 ? Constants.ApplicationSettings.DefaultValues.PageSize : pageSize.Value;
            TotalItems = query.Count();
            TotalPages = TotalItems / PageSize;

            if (TotalItems % PageSize == 0)
            {
                TotalPages++;
            }

            Source = query.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }

        public PagedList(IEnumerable<T> query, int? pageIndex = null, int? pageSize = null)
        {
            PageIndex = pageIndex ?? 1;
            PageSize = !pageSize.HasValue || pageSize == 0 ? Constants.ApplicationSettings.DefaultValues.PageSize : pageSize.Value;
            TotalItems = query.Count();
            TotalPages = TotalItems / PageSize;

            if (TotalItems % PageSize == 0)
            {
                TotalPages++;
            }

            Source = query.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }

        public IList<T> Source { get; }
        public int PageIndex { get; }
        public int PageSize { get; }
        public int TotalItems { get; }
        public int TotalPages { get; }
    }
}