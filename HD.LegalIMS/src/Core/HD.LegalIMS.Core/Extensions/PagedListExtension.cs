﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HD.LegalIMS.Core.Shared;

namespace HD.LegalIMS.Core.Extensions
{
    public static class PagedListExtension
    {
        public static PagedList<T> ToPagedList<T>(this IQueryable<T> query, int? pageIndex = null, int? pageSize = null)
        {
            pageIndex ??= 1;
            pageSize = !pageSize.HasValue || pageSize == 0 ? Constants.ApplicationSettings.DefaultValues.PageSize : pageSize.Value;
            int totalItems = query.Count();

            IEnumerable<T> source = query.Skip((pageIndex.Value - 1) * pageSize.Value)
                .Take(pageSize.Value).AsEnumerable();
            return new PagedList<T>(source, totalItems);
        }

        public static async Task<PagedList<T>> ToPagedListAsync<T>(this IQueryable<T> query, int? pageIndex = null, int? pageSize = null)
        {
            pageIndex ??= 1;
            pageSize = !pageSize.HasValue || pageSize == 0 ? Constants.ApplicationSettings.DefaultValues.PageSize : pageSize.Value;
            int totalItems = query.Count();

            List<T> source = await query
                .Skip((pageIndex.Value - 1) * pageSize.Value)
                .Take(pageSize.Value)
                .ToListAsync();

            return new PagedList<T>(source, pageIndex, pageSize);
        }
    }
}