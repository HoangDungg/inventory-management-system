using HD.LegalIMS.Services.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace HD.LegalIMS.Services.Infrastructure.Data
{
    public class SourceContext : IdentityDbContext<IdentityUser>
    {
        public SourceContext(DbContextOptions<SourceContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfigurationsFromAssembly(typeof(SourceContext).Assembly);
        }

        public override int SaveChanges()
        {
            OnBeforeSaving();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        #region Entities

        public DbSet<Category> Categories { get; set; }

        #endregion Entities

        #region Support Func

        private void OnBeforeSaving()
        {
            System.Collections.Generic.IEnumerable<Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry> entries = ChangeTracker.Entries();
            foreach (Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry entry in entries)
            {
                if (entry.Entity is BaseEntity trackableEntity)
                {
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            trackableEntity.CreatedDate = DateTime.Now;
                            break;

                        case EntityState.Modified:
                            trackableEntity.UpdatedDate = DateTime.Now;
                            break;
                    }
                }
            }
        }

        #endregion Support Func
    }
}