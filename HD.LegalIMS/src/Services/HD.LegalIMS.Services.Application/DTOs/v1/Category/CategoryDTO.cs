﻿namespace HD.LegalIMS.Services.Application.DTOs.v1.Category
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}