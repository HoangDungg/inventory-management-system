﻿using HD.LegalIMS.Application.RequestValidator;

namespace HD.LegalIMS.Services.Application.UseCases.v1.Category.Commands.AddCategory
{
    public class AddCategoryValidator : BaseRequestValidator<AddCategoryRequest>
    {
    }
}