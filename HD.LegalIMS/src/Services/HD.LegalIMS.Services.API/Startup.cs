using System.Linq;
using HD.LegalIMS.EntityFramework;
using HD.LegalIMS.API.Extensions;
using HD.LegalIMS.API.Middleware;
using HD.LegalIMS.Infrastructure;
using HD.LegalIMS.Services.Infrastructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ZWA.Infrastructure.Core;

namespace HD.LegalIMS.Services.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "HD.LegalIMS.Services.API", Version = "v1" });
            });

            services.AddDbContextPool<SourceContext>(opts =>
                    opts.UseSqlServer(Configuration.GetConnectionString("SourceContext")));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<SourceContext>()
                .AddDefaultTokenProviders();

            services.AddCoreDependencies(Configuration.LoadApplicationAssemblies().ToArray());
            services.AddWebAPICore();
            services.AddApiVersioningCore();
            services.AddEntityFrameworkGenericRepositories();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "HD.LegalIMS.Services.API v1"));
            }

            app.UseMiddleware<ExceptionHandlerMiddleware>()
               .UseHttpsRedirection()
               .UseResponseCaching()
               .UseRouting()
               .UseAuthorization()
               .UseAuthentication()
               .UseEndpoints(endpoints =>
               {
                   endpoints.MapControllers();
               });
        }
    }
}